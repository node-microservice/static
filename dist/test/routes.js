"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("ootils");
const assert = require("assert");
const request = require("supertest");
const index_1 = require("../index");
let service;
describe('routes', () => {
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main({ root: 'dist/test' });
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });
    describe('status', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            yield request(service.server).get('/status').expect(200);
        }));
    });
    describe('file', () => {
        it('helloworld.txt', () => __awaiter(this, void 0, void 0, function* () {
            const res = yield request(service.server).get('/fixtures/helloworld.txt').expect(200);
            assert.equal(res.text, 'hello world');
        }));
    });
    describe('benchmark', () => {
        let tmplog = null;
        beforeEach(() => __awaiter(this, void 0, void 0, function* () {
            tmplog = service.log;
            service.log = (level, ...args) => __awaiter(this, void 0, void 0, function* () { });
        }));
        afterEach(() => {
            service.log = tmplog;
        });
        it('< 1KB cached', () => __awaiter(this, void 0, void 0, function* () {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                yield ootils_1.Requests.get(url_self + '/fixtures/helloworld.txt');
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        })).timeout(20000);
        it('< 1KB not cached', () => __awaiter(this, void 0, void 0, function* () {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                yield ootils_1.Requests.get(url_self + '/fixtures/helloworld.txt?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        })).timeout(20000);
        it('root (invalid)', () => __awaiter(this, void 0, void 0, function* () {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                yield ootils_1.Requests.get(url_self + '/?ts=' + new Date().getTime(), { simple: false });
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        })).timeout(20000);
        it('status', () => __awaiter(this, void 0, void 0, function* () {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                yield ootils_1.Requests.get(url_self + '/status?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        })).timeout(20000);
    });
});
