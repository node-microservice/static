import { MicroService, RouteOptions } from 'microservice'
import { Requests } from 'ootils';
import * as assert from 'assert';
import * as mocha from 'mocha';
import * as url from 'url';
import * as request from 'supertest';
import { main } from '../index';
let service: MicroService;

describe('routes', () => {

    before(async () => {
        service = await main({root: 'dist/test'});
    });

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });

    describe('status', () => {

        it('OK', async () => {
            await request(service.server).get('/status').expect(200);
        });
    });

    describe('file', () => {

        it('helloworld.txt', async () => {
            const res = await request(service.server).get('/fixtures/helloworld.txt').expect(200);
            assert.equal(res.text, 'hello world');
        });
    });

    describe('benchmark', () => {
        let tmplog: (level: string, ...args: any[]) => Promise<void> = null;
        beforeEach(async () => {
            tmplog = service.log;
            service.log = async (level: string, ...args: any[]) => {};
        });
        afterEach(() => {
            service.log = tmplog;
        });

        it('< 1KB cached', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/fixtures/helloworld.txt');
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('< 1KB not cached', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/fixtures/helloworld.txt?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('root (invalid)', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/?ts=' + new Date().getTime(), {simple: false});
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('status', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/status?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);
    });
});