import { MicroService } from 'microservice';
import * as minimist from 'minimist';
import * as url from 'url';
import * as express from 'express';

/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 * 
 * `port` - TCP port where this service should run
 * 
 * `db` - database location, defaults to in-memory database
 * 
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
export async function main(opts: {[key: string]: any} = {}): Promise<MicroService> {

    // Parse command-line arguments
    const GLOBAL_STATS: {[key: string]: any} = {};
    const args = minimist(process.argv.slice(1));

    // Helper function used to read arguments. The order of preference is: 1) opts 2) args 3) env
    function getopt(name: string, env = true): string {
        return opts[name] || args[name] || (env && process.env[name]);
    }

    // Initialize microservice
    const service = await MicroService.create(getopt('db'));

    // Setup debugging
    process.on('unhandledRejection', err => service.log('E', err));

    // Routes setup

    // Status
    // Important: do not delete this route if you plan on using orchestrator!
    await service.route('/status', (request, response) => {
        response.send({status: 'OK', data: GLOBAL_STATS});
    });

    // Use the underlying Express instance directly to serve static files
    const loc = getopt('path') || '/';
    const root = getopt('root') || '/var/www';
    service.routes.use(loc, express.static(root))

    // Log parameters for debugging purposes
    service.log('I', `Started static file server at ${root} with relative path ${loc}`)

    // Maintenance loop
    const maintenance_loop = async () => {
        const rows = await service.db.all(
            `SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
        const msgs = rows.map((row: any) => row.message);
        GLOBAL_STATS.last_errors = rows
            .filter((row: any, ix) => msgs.indexOf(row.message) === ix)
            .map((row: any) => `[${row.timestamp}] ${row.message}`);

        // Register with orchestrator if option was passed as argument
        if (getopt('orchestrator')) {
            const orchestrator_url = url.parse(getopt('orchestrator'));
            service.register(orchestrator_url, 'static', '/').catch(err => {});
        }
    };
    setInterval(maintenance_loop, 60000);

    // Pick a random port
    service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8999) + 1000);
    await maintenance_loop();

    // Notify other components of startup, but do not exit on failure
    await service.notify().catch(err => { /* already logged by notify() */ });
    
    return service;
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
