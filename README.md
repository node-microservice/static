# MicroService Static

Static file server built on microservice architecture.

## Installation

Installing this package globally also installs a SystemD template unit file. To start the SystemD service then, run the following commands as sudo:

    npm install --global gitlab:node-microservice/static --unsafe-perm
    systemctl daemon-reload
    systemctl enable microservice-static@1234
    systemctl start microservice-static@1234

Substitute 1234 with the port under which you want to run the microservice static server. The location of the static file server defaults to /var/www but can be changed via environment variables. To do that, create a file under `/etc/microservice/microservice-static.env` with the following contents:

    root=/path/to/directory

Substitute /path/to/directory with the location under which you want to run the static file server.

## Benchmarks

The following are results from benchmark testing on a Macbook Pro 2015:

| Test             | Requests per second |
| ---------------- | ------------------- |
| < 1KB cached     | 866.55              |
| < 1KB not cached | 877.96              |
| 4xx status       | 828.50              |
| /status          | 518.13              |

To run your own benchmarks, simply run: `npm run benchmark`.
